export enum Logement {
  APPARTEMENT = 'Appartement',
  MAISON = 'Maison'
}

export enum Zone {
  H1 = 'H1',
  H2 = 'H2',
  H3 = 'H3'
}
