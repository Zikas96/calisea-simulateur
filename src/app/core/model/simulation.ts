export interface Simulation {
  logement: string;
  zone: string;
  revenue: number;
  surface: number;
}
