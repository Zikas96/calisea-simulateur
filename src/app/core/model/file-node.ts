export interface FileNode {
  designation: string,
  level: number,
  children?: FileNode[],
  code?: string,
  subTitle?: string,
  path?: string
}
