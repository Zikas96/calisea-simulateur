import { Injectable } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { FileNode } from '../model/file-node';
import { ApiService } from './api.service';

@Injectable({
  providedIn: 'root'
})
export class FileNodeService {

  fileSub: Subscription;
  private getSedaUrl = './assets/tree.json';

  constructor(private apiService: ApiService) { }


  getTreeFile(): Observable<FileNode>{
    return this.apiService.getLocally<FileNode>(this.getSedaUrl);
}
}
