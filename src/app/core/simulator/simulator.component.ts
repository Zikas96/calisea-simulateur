import { Component, OnInit } from '@angular/core';
import { Logement, Zone } from '../model/logement.enum';
import { Simulation } from '../model/simulation';

@Component({
  selector: 'app-simulator',
  templateUrl: './simulator.component.html',
  styleUrls: ['./simulator.component.scss']
})
export class SimulatorComponent implements OnInit {

  logementTypes: string[] = this.getLogementType();
  selectedLogement: string;
  zones: string[] = this.getZones();
  selectedZone: string;
  simulation: Simulation;
  surface = 0.00;
  revenue = 0.00;

  constructor() { }

  ngOnInit(): void {
    this.simulation = {
      logement: '',
      zone: '',
      revenue: 0.00,
      surface: 0.00
    };
  }

  getLogementType(): string[]{
    return Object.values(Logement);
  }

  getZones(): string[]{
    return Object.values(Zone);
  }

  simulate(): void{
    console.log(this.simulation)
  }

  selectZone(event: any): void{
      this.simulation.zone = event.target.value;
  }

  selectLogement(event: any): void{
      this.simulation.logement = event.target.value;
  }

  changeRevenue(event: any): void{
      this.simulation.revenue = event.target.value;
  }

  changeSurface(event: any): void{
      this.simulation.surface = event.target.value;
  }

}
