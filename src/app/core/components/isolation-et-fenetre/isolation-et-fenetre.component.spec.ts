import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IsolationEtFenetreComponent } from './isolation-et-fenetre.component';

describe('IsolationEtFenetreComponent', () => {
  let component: IsolationEtFenetreComponent;
  let fixture: ComponentFixture<IsolationEtFenetreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IsolationEtFenetreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IsolationEtFenetreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
