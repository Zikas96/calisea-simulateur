import { Component, OnInit } from '@angular/core';
import { Zone } from '../../model/logement.enum';
import { Simulation } from '../../model/simulation';

@Component({
  selector: 'app-isolation-et-fenetre',
  templateUrl: './isolation-et-fenetre.component.html',
  styleUrls: ['./isolation-et-fenetre.component.scss']
})
export class IsolationEtFenetreComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.error(this.simulate({
      logement: '',
      revenue: 1200,
      surface: 65,
      zone: 'H1'
    }));
  }

  simulate(simulationValue: Simulation): number{

    if(simulationValue.zone == Zone.H1){
      return simulationValue.surface * 1700;
    }
    if(simulationValue.zone == Zone.H2){
      return simulationValue.surface * 1400;
    }
    if(simulationValue.zone == Zone.H3){
      return simulationValue.surface * 900;
    }
    return 0;
  }

}
