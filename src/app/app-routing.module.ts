import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsolationEtFenetreComponent } from './core/components/isolation-et-fenetre/isolation-et-fenetre.component';
import { SimulatorComponent } from './core/simulator/simulator.component';
import { HomeComponent } from './home/home.component';
import { CardComponent } from './shared/card/card.component';
import { NotFoundComponent } from './shared/not-found/not-found.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'simul', component: SimulatorComponent},
  {path: 'card', component: CardComponent},
  {path: 'isolation-et-fenetre', component: IsolationEtFenetreComponent},
  {path: '**', component: NotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
