import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SimulatorConfiguration } from './core/model/simulator-configuration';
import { ApiService } from './core/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class AppRoutingService {

  constructor(private apiService: ApiService) { }

  public initConfiguration(): Observable<SimulatorConfiguration> {
    return this.apiService.getLocally("./assets/config.json");
  };
}
