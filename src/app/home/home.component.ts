import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FileNode } from '../core/model/file-node';
import { FileNodeService } from '../core/services/file-node.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  fileTree: FileNode[];
  types: string[];

  constructor(private fileService: FileNodeService, private router:Router) { }

  ngOnInit(): void {
    this.getFile();
  }

  getFile(){
    this.fileService.getTreeFile().subscribe(
      (value: FileNode) => {
        if(value.children){
          this.fileTree = value.children;
        }
      },
      (error) => {
        console.log(error)
      }
    )
  }

  getChildren(node: FileNode){
    if(node.children && node.children?.length != 0 ){
      this.fileTree = node.children;
    }
    if(node.level == 3){
      this.router.navigateByUrl(node.path ? node.path : '/');
    }
  }

}
